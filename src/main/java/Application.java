import beans.EnglishSpeaker;
import beans.FrenchSpeaker;
import beans.Person;
import config.AppConfig;
import interfaces.Speaker;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class Application {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.register(AppConfig.class);
        context.refresh();

//        Speaker speakerFrench = context.getBean(Speaker.class);
//        speakerFrench.sayHello();
//
//        Speaker speakerEnglish = context.getBean(EnglishSpeaker.class);
//        speakerEnglish.sayHello();
//
//        Speaker englishSpeaker = context.getBean(FrenchSpeaker.class);
//        System.out.println(englishSpeaker.sayRandomWord());

        Person person = (Person) context.getBean("bothLanguages");
        person.sayHello();
        System.out.println(person.sayRandomWord());
        context.close();
//        RandomNumber number2 = context.getBean(RandomNumber.class);
//        System.out.println(number2.getNumber());
//        Writer fileWriter = context.getBean(FileWriter.class);
//        Writer consoleWriter = context.getBean(ConsoleWriter.class);
//        fileWriter.writeText("Scrie");
//        consoleWriter.writeText("Scrie");
    }
}