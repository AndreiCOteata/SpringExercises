package beans;

import interfaces.Speaker;
import interfaces.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.util.List;

public class FrenchSpeaker implements Speaker {

    private List<String> words;

    private RandomNumber randomNumber;

    private Writer writer;

    @Autowired
    public FrenchSpeaker(@Qualifier("consoleWriter") Writer writer, List<String> words, RandomNumber randomNumber) {
        this.writer = writer;
        this.words = words;
        this.randomNumber = randomNumber;
    }

    @Override
    public String sayRandomWord() {
        return words.get((int) this.randomNumber.getNumber());
    }

    @Override
    public void sayHello() {
        this.writer.writeText("bonjour");
    }
}
