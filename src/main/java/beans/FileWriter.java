package beans;

import interfaces.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.File;
import java.io.IOException;

public class FileWriter implements Writer {

    private final String fileName;

    @Autowired
    public FileWriter(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public void writeText(String text) {
        try {
            File myObj = new File(this.fileName);
            if (myObj.createNewFile()) {
                System.out.println("File created: " + myObj.getName());
            } else {
                System.out.println("File already exists.");
            }
            try {
                java.io.FileWriter myWriter = new java.io.FileWriter(this.fileName);
                myWriter.write(text);
                myWriter.close();
                System.out.println("Successfully wrote to the file.");
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }
}
