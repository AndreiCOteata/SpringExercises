package beans;

import interfaces.Speaker;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Person implements Speaker {

    private final List<Speaker> languages;

    public Person(Speaker... speakers){
        this.languages = new ArrayList<>(Arrays.asList(speakers));
    }

    @Override
    public String sayRandomWord() {
        StringBuilder words = new StringBuilder();
        for (Speaker each: languages) {
            words.append(" ").append(each.sayRandomWord());
        }
        return words.toString();
    }

    @Override
    public void sayHello() {
        for (Speaker each: languages) {
            each.sayHello();
        }
    }
}
