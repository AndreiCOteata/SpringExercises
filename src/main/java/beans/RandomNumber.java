package beans;


public class RandomNumber {
    private long number;

    public RandomNumber(long number){
        this.number = number;
    }

    public long getNumber() {
        return number;
    }
}
