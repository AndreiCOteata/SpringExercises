package beans;

import interfaces.Speaker;
import interfaces.Writer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;

public class EnglishSpeaker implements Speaker {

    private List<String> words;

    private RandomNumber randomNumber;

    private final Writer writer;

    @Autowired
    public EnglishSpeaker(@Qualifier("fileWriter") Writer writer, List<String> words, RandomNumber randomNumber) {
        this.writer = writer;
        this.words = words;
        this.randomNumber = randomNumber;
    }

    @Override
    public String sayRandomWord() {
        return words.get((int)randomNumber.getNumber());
    }

    @Override
    public void sayHello() {
        this.writer.writeText("Bunica!");
    }

    @PostConstruct
    public void init(){
        System.out.println("Initializing!");
    }

    @PreDestroy
    public void cleanUp(){
        System.out.println("Cleanning up!");
    }
}
