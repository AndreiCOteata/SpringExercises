package config;

import beans.*;
import interfaces.Writer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import java.util.Collections;

@Configuration
@ComponentScan("beans")
public class AppConfig {
    @Bean
    @Scope("prototype")
    public RandomNumber randomNumber(){
        return new RandomNumber((long) (Math.random() * 100));
    }

    @Bean
    @Scope("singleton")
    public FileWriter fileName(){
        return new FileWriter("filename.txt");
    }

    @Bean
    @Scope("prototype")
    public EnglishSpeaker englishWriterSpeaker(){
        return new EnglishSpeaker(this.fileName(), new SpeakerProperty("EnglishWords.txt").getStringList(), this.randomNumber());
    }

    @Bean
    @Scope("prototype")
    public FrenchSpeaker frenchWriterSpeaker(){
        return new FrenchSpeaker(this.fileName(), new SpeakerProperty("FrenchWords.txt").getStringList(), this.randomNumber());
    }

    @Bean
    @Scope("prototype")
    public Person englishSpeaker(){
        return new Person(this.englishWriterSpeaker());
    }

    @Bean
    @Scope("prototype")
    public Person frenchSpeaker(){
        return new Person(this.frenchWriterSpeaker());
    }

    @Bean
    @Scope("prototype")
    public Person bothLanguages(){
        return new Person(this.frenchWriterSpeaker(), this.englishWriterSpeaker());
    }
}
