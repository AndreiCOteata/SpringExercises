package interfaces;

public interface Writer {
    void writeText(String text);
}
