package interfaces;

import beans.SpeakerProperty;

public interface Speaker {
    String sayRandomWord();

    void sayHello();
}
